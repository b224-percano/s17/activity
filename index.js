/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printWelcomeMessage() {
			let fullName = prompt("Enter Your Fullname: ");
			let age = prompt("Enter Your Age: ");
			let location = prompt("Enter Your Location: ")

			console.log("Hello, I am " + fullName + "!");
			console.log("I am " + age + " years old!");
			console.log("I lived in " + location + ".");
};
printWelcomeMessage();


function showBands() {
	let band1 = "1. Eraserheads"
	let band2 = "2. Kamikazee"
	let band3 = "3. Mayonnaise"
	let band4 = "4. Parokya ni Edgar"
	let band5 = "5. Rivermaya"

	console.log(band1);
	console.log(band2);
	console.log(band3);
	console.log(band4);
	console.log(band5);
	
}
showBands();

function showMovies() {
	let movie1 = "1. Avengers: Endgame"
	let movie2 = "2. Avengers: Infinity War"
	let movie3 = "3. Spiderman No Way Home"
	let movie4 = "4. Thor Ragnarok"
	let movie5 = "5. The Greatest Showman"

	let descMov1 = "Rotten Tomatoes Rating: 94%"
	let descMov2 = "Rotten Tomatoes Rating: 85%"
	var descMov3 = "Rotten Tomatoes Rating: 93%"
	var descMov4 = "Rotten Tomatoes Rating: 93%"
	var descMov5 = "Rotten Tomatoes Rating: 86%"

	console.log(movie1);
	console.log(descMov1);
	console.log(movie2);
	console.log(descMov2);
	console.log(movie3);
	console.log(descMov3);
	console.log(movie4);
	console.log(descMov4);
	console.log(movie5);
	console.log(descMov5);
};
showMovies();

console.log("");
function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3);
	alert("Thank you for your input!"); 
};
printUsers();


